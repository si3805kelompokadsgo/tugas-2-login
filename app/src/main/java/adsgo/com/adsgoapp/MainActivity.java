package adsgo.com.adsgoapp;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {
    Button loginBtn, cancelBtn;
    EditText userField, passField;
    //TextView loginJudul, userTxt, passTxt;

    //int counter = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        cancelBtn = (Button) findViewById(R.id.cancelBtn);

        userField = (EditText) findViewById(R.id.userField);
        passField = (EditText) findViewById(R.id.passField);

        loginBtn.setOnClickListener(new View.OnClickListener(){
            if(userField.getText().toString().equals("admin")&& passField.getText().toString().equals("admin")){
                Intent intentDetail = new Intent(MainActivity.this, DetailActivity.class);
                startActivity(intentDetail);
            }
            else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity);
                dialog.setTitle("Username/Password salah");
                dialog.create().show();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
    }
}
